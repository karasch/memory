# Memory

A simple memory / card matching game using Python and pygame.

Click on the cards, and they will be removed if they match.  If they do not match, they will be flipped back over.  The goal is to remove all the cards.

## Running the game

```
pip install -r requirements.txt
python memory.py
```

## TODO List
* Add a game over message
* Keep track of the number of moves for the scoreboard

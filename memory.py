import random, sys, pygame
from cardback import CardBack
from cardfront import CardFront
from pygame.locals import *

BACKGROUNDCOLOR = pygame.Color('white')
CARDBACK_COLOR = pygame.Color('green')
CARDBORDER_COLOR = pygame.Color('red')
CARDFRONT_COLOR = pygame.Color('blue')
CARDTEXT_COLOR = pygame.Color('white')

SCORE_TEXT_COLOR = pygame.Color('blue')
STATUS_TEXT_COLOR = pygame.Color('red')

# size of the game window, in pixels
WINDOWWIDTH = 480
WINDOWHEIGHT = 530

SCOREBOARD_HEIGHT = 50

# Number of cards across the game
LAYOUT_ACROSS = 6
# Number of cards layed out vertically
LAYOUT_DOWN = 3

def terminate():
    """Exit the running program"""
    pygame.quit()
    sys.exit()


def main():
    """Program execution starts here"""

    # Calculate the size of a card, in pixels
    card_width = WINDOWWIDTH/LAYOUT_ACROSS
    card_height = (WINDOWHEIGHT-SCOREBOARD_HEIGHT)/LAYOUT_DOWN

    # Figure out how many cards we have
    num_cards = LAYOUT_DOWN * LAYOUT_ACROSS
    # You can't have a matching game with an odd number of cards
    if num_cards % 2 != 0:
        print("You can't have an odd number of cards.  Either LAYOUT_DOWN or LAYOUT_ACROSS must be an even number.")
        sys.exit()

    # Initialize pygame
    pygame.init()
    main_clock = pygame.time.Clock()

    # Set up the window
    window_surface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), 0, 32)
    pygame.display.set_caption("Memory")

    # Create the scoreboard
    scoreboard = pygame.Surface((WINDOWWIDTH, SCOREBOARD_HEIGHT))

    # Create the game board
    gameboard = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT-SCOREBOARD_HEIGHT))
    gameboard.get_rect().y = SCOREBOARD_HEIGHT

    status_text = ""

    # Create the sprite Groups we will need.
    card_backs = pygame.sprite.Group()
    card_fronts = pygame.sprite.Group()
    flipped_card_backs = pygame.sprite.Group()
    flipped_card_fronts = pygame.sprite.Group()

    # Create some values to use for the cards
    card_values = [*range(int(num_cards/2))]
    card_values.extend([*range(int(num_cards/2))])
    random.shuffle(card_values)

    # Create the cards
    for x in range(LAYOUT_ACROSS):
        for y in range(LAYOUT_DOWN):
            card = CardBack(card_width, card_height, card_width*x, card_height*y, CARDBACK_COLOR, CARDBORDER_COLOR)
            card.add(card_backs)
            card_front = CardFront(card_width, card_height, card_width*x, card_height*y, str(card_values.pop()), CARDFRONT_COLOR, CARDTEXT_COLOR)
            card_front.add(card_fronts)

    # State variables for the main loop
    # Stores the last card turned face up, unless it has been removed or turned face down
    already_clicked = None
    # Are we looking at flipped cards, or flipping new cards?
    viewing_flipped_cards = False
    # Did the last set match?
    last_set_matched = False

    # The main loop
    while True:
        # React to input
        for event in pygame.event.get():
            # Terminate cleanly if the X is clicked
            if event.type == pygame.locals.QUIT:
                terminate()
            # React to mouse clicks
            if event.type == pygame.locals.MOUSEBUTTONUP:
                # If we're just looking at the cards, clean them up
                if viewing_flipped_cards:
                    if last_set_matched:
                        #remove the cards that were flipped
                        for i in flipped_card_backs.sprites():
                            i.kill()
                        for i in flipped_card_fronts.sprites():
                            i.kill()
                    else:
                        # flip the cards back over
                        card_backs.add(flipped_card_backs.sprites())
                        flipped_card_backs.empty()
                        card_fronts.add(flipped_card_fronts.sprites())
                        flipped_card_fronts.empty()

                    # Go back to searching mode
                    viewing_flipped_cards = False
                    status_text = ""
                else:
                    # If we're flipping cards over, check whether we have clicked a card
                    pos = pygame.mouse.get_pos()
                    # NOTE: The sprite.rect reports its position relative to the Surface,
                    # so we have to adjust the position of the mouse event in order to compare.
                    clicked_cardbacks = [s for s in card_backs if s.rect.collidepoint((pos[0], pos[1]-SCOREBOARD_HEIGHT))]
                    clicked_cardfronts = [s for s in card_fronts if s.rect.collidepoint((pos[0], pos[1]-SCOREBOARD_HEIGHT))]


                    for i in clicked_cardbacks:
                        # If the card was not previously flipped over, move the cardback out of the Group we'll draw
                        # But keep track of it, because we'll need it later.
                        i.add(flipped_card_backs)
                        i.remove(card_backs)

                    for i in clicked_cardfronts:
                        # If the card was not previously flipped over, move the card front to the Group will draw
                        i.add(flipped_card_fronts)
                        i.remove(card_fronts)
                        if already_clicked is None:
                            #print(f"Found card {i.character}")
                            already_clicked = i
                        else:
                            if already_clicked.character == i.character:
                                status_text = "Match!"
                                #print("Match!")
                                last_set_matched = True
                                # Update the number of cards remaining
                                num_cards=num_cards-2
                            else:
                                status_text = f"No match!"
                                #print(f"No match: {already_clicked.character} {i.character}")
                                last_set_matched = False

                            already_clicked = None
                            viewing_flipped_cards = True

        # Fill in the surfaces
        gameboard.fill(BACKGROUNDCOLOR)
        scoreboard.fill(BACKGROUNDCOLOR)

        # Draw the scoreboard
        font = pygame.font.SysFont(None, 36)
        score_text = font.render(f"Cards Remaining: {num_cards}", 1, SCORE_TEXT_COLOR)
        score_rect = score_text.get_rect()
        score_rect.centery=int(SCOREBOARD_HEIGHT/2)
        scoreboard.blit(score_text, score_rect)

        score_status = font.render(status_text, 1, STATUS_TEXT_COLOR)
        status_rect = score_status.get_rect()
        status_rect.centery=int(SCOREBOARD_HEIGHT/2)
        status_rect.right = WINDOWWIDTH
        scoreboard.blit(score_status, status_rect)        

        # Draw the cards
        card_backs.draw(gameboard)
        flipped_card_fronts.draw(gameboard)

        # Draw the surfaces on the window
        window_surface.blit(gameboard, (0, SCOREBOARD_HEIGHT))
        window_surface.blit(scoreboard, (0,0))

        pygame.display.update()
        main_clock.tick(40)

if __name__ == '__main__':
    main()

import pygame

class CardFront(pygame.sprite.Sprite):
    def __init__(self, width, height, x_pos, y_pos, character, card_color=pygame.Color('blue'), text_color=pygame.Color('white')):
        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill(card_color)

        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos

        font = pygame.font.SysFont(None, 48)
        textObj = font.render(character, 1, text_color)
        text_width = textObj.get_width()
        text_height = textObj.get_height()
        self.image.blit(textObj, [width/2 - text_width/2, height/2 - text_height/2])

        self.character = character

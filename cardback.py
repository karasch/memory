import pygame

class CardBack(pygame.sprite.Sprite):
    def __init__(self, width, height, x_pos, y_pos, back_color=pygame.Color('green'), border_color=pygame.Color('red'), border_width=3):
        super().__init__()
        # Draw the border by filling the background with the border color, then painting a background-colored rectangle over it.
        self.image = pygame.Surface([width, height])
        self.image.fill(border_color)

        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos

        # Remember, these x,y coordinates are relative to the sprite we're drawing on.
        self.back = pygame.Rect((border_width, border_width),  (width-(border_width*2), height-(border_width*2)))
        pygame.draw.rect(self.image, back_color, self.back)
